# python script to plot simple dust advection test
import numpy as np 
import matplotlib.pylab as plt 

file_index="00270"

#constants
mh=1.673532638056825e-24
kb=1.3806488e-16
Msun=1.9891e33
Guniv=6.673848e-8
# Numerical parameters from simulation
R0=1.5e13
T_0=100
mmw=2.5
nu_PL=1.0
alpha=0.01
Mstar=Msun
Rin=1.5e12
Mdot=1e17

# calculate visosity
cs_0=(kb*T_0/(mmw*mh))**0.5
Omega_0=(Guniv*Mstar/(R0**3.0))**0.5
nu_0=alpha*cs_0**2.0/Omega_0

#load data
filename="gas%s.out" %(file_index)
data=np.loadtxt(filename) 
Time=data[0,0]
R=data[1:,0]
S=data[1:,1]

# Analytic solution nu*Sigma=Mdot/(3*pi)*(1-sqrt(Rin/R))
nu=nu_0*(R/R0)**nu_PL
S_analytic=Mdot/(3*np.pi*nu)*(1-(Rin/R)**0.5)

plt.loglog(R,S,'o')
plt.loglog(R,S_analytic,'r')
plt.xlim((1e12,2e16))
plt.xlabel('Radius [cm]')
plt.ylabel('Surface Density [g/cm2]')
Title="Surface Density at %g years" %(Time/31556926)
plt.title(Title)
plt.ylim((1e-2,1e3))
plt.show()
