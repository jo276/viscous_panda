# python script to plot LBP74 solution against numerical result
import numpy as np 
import matplotlib.pylab as plt 

file_index="00110"

#constants
mh=1.673532638056825e-24
kb=1.3806488e-16
Msun=1.9891e33
Guniv=6.673848e-8
# Numerical parameters from simulation
R0=1.5e13
T_0=100
mmw=2.5
nu_PL=1.0
alpha=0.01
Mstar=Msun
Md0=1e30
R1=1.5e14
#calculate viscous time at R1
cs_0=(kb*T_0/(mmw*mh))**0.5
Omega_0=(Guniv*Mstar/(R0**3.0))**0.5
nu_0=alpha*cs_0**2.0/Omega_0
nu_1=nu_0*(R1/R0)**nu_PL
t_nu=R1**2.0/(3.0*nu_1)
#load data
filename="gas%s.out" %(file_index)
data=np.loadtxt(filename) 
Time=data[0,0]
R=data[1:,0]
S=data[1:,1]

#load data at zero time

filename="gas00000.out"
data=np.loadtxt(filename)
S0=data[1:,1]
S_analytic_0=Md0/(2.0*np.pi*R*R1)*np.exp(-R/R1)

T=1.0+Time/t_nu
S_analytic=Md0/(2.0*np.pi*R*R1)*(1/T**1.5)*np.exp(-R/(R1*T))

plt.loglog(R,S_analytic)
plt.loglog(R,S,'o')

plt.loglog(R,S0,'o')
plt.loglog(R,S_analytic_0)
plt.xlim((1e12,1e19))
plt.xlabel('Radius [cm]')
plt.ylabel('Surface Density [g/cm2]')
Title="Surface Density at 0 & %g years" %(Time/31556926)
plt.title(Title)
plt.ylim((1e-12,1e4))
plt.show()
