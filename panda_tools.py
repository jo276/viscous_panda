# collection of input scripts for panda dust evolution code

class gas_data(object):
	time = 0.
	radius = []
	surfdens =[]

class dust_data(object):
	time = 0.
	radius =[]
	surfdens =[]

class birnstiel_data(object):
	time =0.
	monomer_size =0.
	rho_dust =0.	
	radius =[]
	amax=[]
	afrag=[]
	adrift=[]
	adf=[]
	agrow=[]
	fm=[]

class growing_planet(object):
	time =0.
	mass=[]
	mdot=[]
	sep=[]
	disc_mass=[]


def read_gas(n):
	import numpy as np 
	# assumes you're in top level directory
	filename="output/gas{0:05}.out".format(n)
	data=np.loadtxt(filename)
	gas=gas_data()
	gas.time=data[0,0]
	gas.radius=data[1:,0]
	gas.surfdens=data[1:,1]

	return gas

def read_dust(n,s):
	import numpy as np
	filename="output/dust{0:05}s{1:02}.out".format(n,s)
	data=np.loadtxt(filename)
	dust=dust_data()
	dust.time=data[0,0]
	dust.radius=data[1:,0]
	dust.surfdens=data[1:,1]

	return dust

def read_birnstiel(n):
	import numpy as np
	filename="output/birnstiel{0:05}.out".format(n)
	data=np.loadtxt(filename)
	birnstiel=birnstiel_data()
	birnstiel.time=data[0,0]
	birnstiel.monomer_size=data[0,2]
	birnstiel.rho_dust=data[0,3]
	birnstiel.radius=data[1:,0]
	birnstiel.amax=data[1:,1]
	birnstiel.afrag=data[1:,2]
	birnstiel.adrift=data[1:,3]
	birnstiel.adf=data[1:,4]
	birnstiel.agrow=data[1:,5]
	birnstiel.fm=data[1:,6]

	return birnstiel

def read_growing_planet():
	import numpy as np
	filename="output/Planet.out"
	data=np.loadtxt(filename)
	planet=growing_planet()
	planet.time=data[:,0]
	planet.mass=data[:,1]
	planet.mdot=data[:,2]
	planet.sep=data[:,3]
	planet.disc_mass=data[:,4]

	return planet	