!cccccccccccc PANDA 1D viscous evolution code cccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   GRID MODULE: HOLDS GRID VARIBLES AND GENERATES GRID
!c
!c   last modified by JO: 10-07-2014
!c   
!c   v0.1
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


  module grid_mod

    implicit none

    ! Define grid variables
    !real scalars
    double precision                 :: Rmin,Rmax !in and out boundaries
    double precision                 :: grid_fact ! grid size variation for PL grid
    
    !logical

    !integer scalars
    integer                          :: NR !number of radial boundaries
    integer                          :: is,ie !starting and ending index of b grid
    integer                          :: grid_type ! type of grid to use 0 - Power-law, 1 - logarthmic


    !real vectors
    double precision, dimension(:), allocatable, save  ::  Ra,Rb
    double precision, dimension(:), allocatable, save  ::  dRa,dRb,DVa,DVb ! difference vectors
    double precision, dimension(:), allocatable, save  ::  g2a,g2b ! area elements

    contains

    subroutine make_grid
      
      implicit None


      integer                       :: i,j,k
      

      double precision              :: Rstart, Rend
      double precision              :: DlogR ! difference in log grid

      if (grid_type .eq. 0) then
        !power-law grid
        Rstart=Rmin**(1./grid_fact)
        Rend=Rmax**(1./grid_fact)
      elseif (grid_type .eq. 1) then
        !logarthmic grid
        Rstart=log10(Rmin)
        Rend  =log10(Rmax)
      else
        !different grid option        
        stop "Grid_option Incorrectly Specified, stopped in make_grid"
      endif

      DlogR=(Rend-Rstart)/NR

      !assign is & ie
      is=3
      ie=NR+2

      !allocate the grid variables
      allocate(Ra(NR+5))
      allocate(Rb(NR+5))

      ! build mother grid
      Ra(2)=Rstart-DlogR
      Ra(1)=Ra(2)-DlogR
      Ra(ie+2)=Rend+DlogR
      Ra(ie+3)=Ra(ie+2)+DlogR

      do i=is,ie+1
         Ra(i)=Ra(i-1)+DlogR
      end do
        
      do i=is-2,ie+3
         Rb(i)=Ra(i)+DlogR/2
      end do

      if (grid_type .eq. 0) then
        do i=is-2,ie+3
           Ra(i)=Ra(i)**grid_fact
           Rb(i)=Rb(i)**grid_fact
        end do
      elseif (grid_type .eq. 1) then
        do i=is-2,ie+3
          Ra(i)=10.0**Ra(i)
          Rb(i)=10.0**Rb(i)
        end do 
      endif

     !now grid differences and areas
     allocate(dRa(NR+5))
     allocate(dRb(NR+5))
     allocate(DVa(NR+5))
     allocate(DVb(NR+5))
     allocate(g2a(NR+5))
     allocate(g2b(NR+5))

     do i=is-2,ie+2
        dRa(i)=Ra(i+1)-Ra(i)
        DVa(i)=(Ra(i+1)**2.0-Ra(i)**2.0)/2.0
     end do
         
     do i=is-1,ie+3
        dRb(i)=Rb(i)-Rb(i-1)
        DVb(i)=(Rb(i)**2.0-Rb(i-1)**2.0)/2.0
     end do
      
     do i=is-2,ie+3
        g2a(i)=Ra(i)
        g2b(i)=Rb(i)
     end do

     return
               
    end subroutine make_grid



 end module grid_mod
