!ccccccccccccccccc 1D viscous evolution code ccccccccccccccccc
!c
!c   written by JO July 2014 
!c
!c   user module: where user sets up problem
!c
!c   v0.1 modified by JO 3/08/2014
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

	module user_mod

		implicit none

		! users can store extra variables here
	contains

		subroutine user_setup

			use var_mod
			use grid_mod
			use constants

			implicit none

			integer      ::    i
			double precision ::  Md0, R1


			!subroutine where user sets up initial conditions

			namelist /user/ x_ctrl, x_integer_ctrl, x_logical_ctrl

			!read inputs
			open(999,file='input/input.in',status='OLD',recl=100)
			read(999,nml=user)
			close(999)

			!setup Lynden-Bell & Pringel zero time similarity solution 
			!Lynden-Bell & Pringle (1974)

			! get input paramters
			Md0=x_ctrl(1) !initial disc mass
			R1=x_ctrl(2) ! initial scale radius

			do i=is,ie 
				s_gas(i)=Md0/(2*pi*Rb(i)*R1)*exp(-Rb(i)/R1)
				if (s_gas(i) .lt. s_base) s_gas(i)=s_base ! to prevent underflow 
			end do

		end subroutine user_setup

		subroutine user_viscosity

			implicit none

			!subroutine where user specifies user viscosity

		end subroutine user_viscosity

		subroutine user_source_term_g

			implicit none

			!subroutine where user inputs specific source term

		end subroutine user_source_term_g

		subroutine user_source_term_d

			implicit none

			! subroutine where user inputs specific source term

		end subroutine user_source_term_d

		subroutine user_inner_BC_g

			implicit none

			!subroutine where user inputs specific inner BC (gas)

		end subroutine user_inner_BC_g

		subroutine user_outer_BC_g

			implicit none

			!subroutine where user inputs specific outer BC (gas)

		end subroutine user_outer_BC_g

		subroutine user_inner_BC_d

			implicit none

		end subroutine user_inner_BC_d

		subroutine user_outer_BC_d

			implicit none

		end subroutine user_outer_BC_d

		subroutine user_output

			implicit none

			!subroutine where user inputs specific output

		end subroutine user_output

		subroutine user_timestep

			use var_mod

			implicit none

			!subroutine where user can input extra time-step criteria

			dt_user=1d50

		end subroutine user_timestep

		subroutine user_extra_evolve

			implicit none

			!subrotine where user can update anything during a time-step

		end subroutine user_extra_evolve

	end module user_mod