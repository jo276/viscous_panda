!cccccccccccc PANDA 1D viscous evolution code cccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   CONSTANTS MODULE: HOLDS PHYSICAL CONSTANTS [cgs]
!c
!c   modified by JO 10-07-2014 
!c
!c   v0.1
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


  module constants

    !SPLIT INTO TWO PARTS FIXED CONTANTS AND DEFINABLE CONSTANTS

    !FIXED-------------------------------------------------------------
    
    !physical constants in cgs
    double precision, parameter   ::  Guniv=6.673848d-8 !Gravitational constant
    double precision, parameter   ::  stefan=5.6704d-5 !Stefan boltzmann constant
    double precision, parameter   ::  kb=1.3806488d-16 !boltzmann constant
    double precision, parameter   ::  hplank=6.626d-27 !planks constant
    double precision, parameter   ::  pi=3.141592653589793 !pi!
    double precision, parameter   ::  mh=1.673532638056825d-24 !hydrogen mass
    double precision, parameter   ::  clight=2.99792458e10 !speed of light
    
    !unit converts to cgs
    double precision, parameter   ::  Msun=1.9891d33 !solar mass
    double precision, parameter   ::  au_to_cm=1.496d13 !1AU in cm
    double precision, parameter   ::  sec_to_year=31557600 !seconds in a year
    double precision, parameter   ::  Mjup=1.898d30 !jupiter mass
    double precision, parameter   ::  Rjup=6.9911e9  !jupiter radius
    double precision, parameter   ::  Mearth=5.972e27 ! earth mass
    double precision, parameter   ::  Rearth=6.371e8 !earth radius
    double precision, parameter   ::  Lsun=3.846d33 !solar luminosity
    double precision, parameter   ::  Rsun=6.96342d10 !solar radius 
    double precision, parameter   ::  Tsun=5778  !effective T of sun

    !------------------------------------------------------------------

    !DEFINABLE---------------------------------------------------------

    !initialise to standard units
    !gas parameters
    double precision              :: gamma=7./5.
    double precision              :: mmw=2.35

    !stellar properties
    double precision              :: Mstar=Msun 
    double precision              :: Tstar=Tsun
    double precision              :: Rstar=Rsun

    !planet properties
    double precision              :: sep=au_to_cm
    double precision              :: Mplanet=Mjup
    double precision              :: Rplanet=Rjup

    !numerical floors
    double precision              :: s_base =1d-20
    double precision              :: sd_base=1d-30

  end module constants
