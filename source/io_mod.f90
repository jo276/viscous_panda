!ccccccccccccccccc 1D viscous evolution code ccccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   input module
!c
!c   v0.1 modified by JO 4/4/15
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


  module io_mod

    use var_mod
    use grid_mod
    use constants
    use user_mod

    implicit none

    integer             ::  Ngas       =0
    integer             ::  Ndust      =0
    integer             ::  Ntemp      =0
    integer             ::  Nbirnstiel =0

    contains

      subroutine read_inputs

        implicit none
        
        integer            :: k

        namelist /controls/ use_planet, use_dust, use_temp, use_birnstiel, Mstar, CFL, adv_method, &
                        Tmax, Nmax, use_birnstiel_with_growth, Mstar
        namelist /grid/ Rmin, Rmax, NR, grid_fact, grid_type
        namelist /IO/ Ndump, user_dump
        namelist /gas/  in_type_g, out_type_g, boundary_in_g, boundary_out_g, alpha, &
                        vis_type, nu_PL, R_0, T_0, nu_0, HoverR_0, s_base, T_type,   &
                        temp_PL,mmw
        namelist /planet/ Mplanet, Rplanet, sep
        namelist /dust/ in_type_d, out_type_d, sd_base, cons_d_dens, boundary_in_d, &
                        boundary_out_d, time_dust, Nsizes, Nspecies, min_dust, max_dust, &
                        dust_PL, SC_cons, amonomer, tgrow_initial, ufrag

        open(999,file='input/input.in',status='OLD',recl=100)
        read(999,nml=controls)
        read(999,nml=grid)
        read(999,nml=IO)
        read(999,nml=gas)
        if (use_planet) then
            read(999,nml=planet)    
        endif
        if (use_dust) then
            read(999,nml=dust)
        endif
        close(999)

        return
      end subroutine read_inputs

      subroutine check_and_do_output(step)

        implicit none

        integer     ::   step

        if (step .eq. 0) then
            !dump to file anyway
            call output_gas
            if (use_planet) then

            endif
            if (use_dust) then
                call output_dust
                if (use_birnstiel) then
                    call output_birnstiel
                endif
            endif
            if (use_temp) then

            endif
            if (user_dump) then
                call user_output
            endif
        else
            !check if step to Ndump
            if (mod(step,Ndump) .eq. 0) then
                if (verbose) then
                    write(*,'(I8.6,2(6e17.9))') step, time/sec_to_year, delta_t/sec_to_year
                endif
                call output_gas
                if (use_planet) then

                endif
                if (use_dust) then
                    call output_dust
                    if (use_birnstiel) then
                        call output_birnstiel
                    endif
                endif
                if (use_temp) then

                endif
                if (user_dump) then
                    call user_output
                endif
            endif
        endif

        return
      end subroutine check_and_do_output


      subroutine output_gas

        ! outputs gas surface density to file
        implicit none

        integer                 :: i
            
        character*128           :: gas_name


        !initial output open file as new
        write(gas_name,'(a,I5.5,a)') 'output/gas',Ngas,'.out'
        open(unit=10, file=gas_name)

        !write time and dt to first line
        write(10,'(2(6e17.9))') Time, delta_t
        do i=is,ie
            write(10,'(2(6e17.9))') Rb(i),s_gas(i)
        end do
        close(10)
        !update counter
        Ngas=Ngas+1

        return
      end subroutine output_gas

      subroutine output_dust

        ! output dust surface densities to file
        implicit none

        integer                 :: i,j,k

        character*128           :: dust_name, format

        do k=1,Nspecies
            !initial output open file as new
            write(dust_name,'(a,I5.5,a,I2.2,a)') 'output/dust',Ndust,'s',k,'.out'
            open(unit=11, file=dust_name)

            !write time and dt to first line
            write(format,'(a,I5,a)') '(',Nsizes+1,'(6e17.9))'
            write(11,'(2(6e17.9))') Time, delta_t
            do i=is,ie
                write(11,format) Rb(i),(s_dust(i,j,k),j=1,Nsizes)
            end do
            close(11)
            !update counter
            Ndust=Ndust+1
        end do
        return
      end subroutine output_dust

      subroutine output_birnstiel

        ! output all parameters related to the Birnstiel et al. (2012) dust evolution model

        implicit none

        integer                   :: i

        character*128             :: dust_name, format

        write(dust_name,'(a,I5.5,a)') 'output/birnstiel',Nbirnstiel,'.out'
        open(unit=12, file=dust_name)

        !write time and dt to first line
        write(12,'(7(6e17.9))') Time, delta_t, amonomer, cons_d_dens, 0., 0., 0.
        do i=is,ie
            write(12,'(7(6e17.9))') Rb(i),size_max(i),afrag(i),adrift(i),adf(i),agrow(i),f_m(i)
        end do
        close(12)
        !update counter
        Nbirnstiel=Nbirnstiel+1

        return

      end subroutine output_birnstiel


      subroutine splash

        !spash screen writes info to screen
        implicit none

        write(*,*) "         SETUP COMPLETE"
        write(*,*) "                       "
        write(*,*) "       o88            88o"
        write(*,*) "      77777.--''''--.77777  "
        write(*,*) "       '88            88' "
        write(*,*) "       .'              `.   "
        write(*,*) "      /                  \   "
        write(*,*) "      |                   |   "
        write(*,*) "      |    pp       pp    |   "
        write(*,*) "      |   88o       o88   |   "
        write(*,*) "     o`.  8    ___    8  .'o "
        write(*,*) "    8888`.     '^'     .'8888  "
        write(*,*) "  o888888o`. `.-'-.' .'o888888o"
        write(*,*) "o888888888oo`-------'oo888BP888o  "
        write(*,*)
        write(*,*) " PANDA 1D VISCOUS EVOLUTION CODE"
        write(*,*) " "
        write(*,*) "   Written by JO, FEB 2013"
        write(*,*) " "
        write(*,*) " v1.0, Open Source Release, Sept '14"
        write(*,*) " "
        write(*,*) "          Dust      (T/F)", use_dust
        write(*,*) "          Planet    (T/F)", use_planet
        write(*,*) "          Temp      (T/F)", use_temp
        write(*,*) "          Birnstiel (T/F)", use_birnstiel
        write(*,*) " "
        write(*,*) "       Max Interations", Nmax
        write(*,*) "       Max Time       ", Tmax/sec_to_year, "[years]"
        write(*,*) " "
        write(*,*) "       Begin Intergration "
        write(*,*) "  Step,   Time [years],     dt [years]"

        return
      end subroutine splash 

  end module io_mod
