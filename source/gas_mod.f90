!ccccccccccccc PANDA 1D viscous evolution code ccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   GAS MODULE: HOLDS GAS VARIABLES AND DOES GAS EVOLUTION
!c
!c   last modified by JO 07-01-2015
!c
!c   v0.9
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


  module gas_mod

    use var_mod
    use grid_mod

    implicit none

    contains


      subroutine alloc_basic_gas

        implicit NONE
        !allocate the basic gas arrays
        allocate(s_gas(NR+5))
        allocate(s_gas_old(NR+5))
        allocate(s_gas_source(NR+5))
        allocate(u_gas(NR+5))
        allocate(viscosity(NR+5))
        allocate(v_kep_a(NR+5))
        allocate(v_kep_b(NR+5))
        allocate(temperature(NR+5))
        allocate(H_g(NR+5))
        return
      end subroutine alloc_basic_gas



      subroutine initialise_gas
        !this subroutine initializes the gas variables and the intial surface density
        !use grid_mod
        use constants

        implicit NONE

        integer                    :: i

        do i=is-2,ie+2
           s_gas(i)       =s_base
           s_gas_old(i)   =s_base
           s_gas_source(i)=0.0
           u_gas(i)       =0.0
           v_kep_a(i)     =sqrt(Guniv*Mstar/Ra(i))
           v_kep_b(i)     =sqrt(Guniv*Mstar/Rb(i))
        end do

        ! now setup temperature if not full temperature solve
        if (use_temp .eqv. .false.) then
           if (T_type .eq. 1) then
            !constant temperature
            temperature=T_0
           elseif (T_type .eq. 2) then
            !power-law T
            do i=is-2,ie+2
              temperature(i)=T_0*(Rb(i)/R_0)**Temp_PL
            end do    
           endif
           !If need temperature defined (e.g.) to run with dust
           !and don't want constant or power-law temp it must be
           !specified by the user in user_setup explicitly
        endif

        return
      end subroutine initialise_gas

      subroutine calc_viscosity

        !this subroutine calculates the kinematic viscosity
        !viscosity is scalar so calculated on b grid
        
        use constants
        use user_mod

        implicit NONE

        integer     :: i

        double precision  :: nu_0,cs_0,Omega_0 !reference parameters

        if (vis_type .eq. 1) then        
          ! constant viscosity
          
          viscosity=nu_0
        elseif (vis_type .eq. 2) then 
          !power-law viscosity

          !calculate reference viscosity
          Omega_0=sqrt(Guniv*Mstar/(R_0**3.0))
          if (T_0 .gt. 0) then
            cs_0=sqrt(kb*T_0/(mmw*mh))
            nu_0=alpha*cs_0**2.0/Omega_0
          else
            nu_0=alpha*(HoverR_0*R_0)**2.0*Omega_0
          endif

          do i=is-2,ie+2
            viscosity(i)=nu_0*(Rb(i)/R_0)**nu_PL
          end do 
        elseif (vis_type .eq. 3) then
          !alpha viscosity

          do i=is-2,ie+2
            cs_0=sqrt(kb*temperature(i)/(mmw*mh))
            Omega_0=sqrt(Guniv*Mstar/(Rb(i)**3.0))
            viscosity(i)=alpha*(cs_0**2.0)/Omega_0
          end do 

        elseif (vis_type .eq. 9) then

          call user_viscosity

        endif

        return

      end subroutine calc_viscosity

      subroutine calc_scale_height

        !this subroutine calculates the gas scale height 
        use constants

        implicit none

        integer          ::       i
        double precision ::       cs_0, Omega_0

        do i=is-2,ie+2
          cs_0=sqrt(kb*temperature(i)/(mmw*mh))
          Omega_0=sqrt(Guniv*Mstar/(Rb(i)**3.0))
          H_g(i)=cs_0/Omega_0
        end do 

        return

      end subroutine calc_scale_height

      subroutine gas_timestep

        !this subroutine calculates the time-step assosiated with gas diffusion

        implicit None

        integer     :: i

        double precision    :: store_dt

        do i=is,ie
           store_dt=dRa(i)**2.0/(6.0*viscosity(i)) !time-step
           if (store_dt .lt. dt_vis) dt_vis=store_dt
        end do

        return
      end subroutine gas_timestep

      subroutine gas_source

        use constants
        use user_mod

        !this subroutine calculates the source and or sink term in the gas equation
        implicit none

        integer    :: i

        if (source_type_g .eq. 0) then
          ! no source term
          s_gas_source=0.
        elseif (source_type_g .eq. 9) then  
          ! user specified
          call user_source_term_g
        endif


        
      end subroutine gas_source

      subroutine advance_gas

        !this subroutine advances the gas density equation

        integer :: i

        double precision, dimension(NR+5)    :: Yg !standard variable for nu*sigma*R^0.5
        double precision, dimension(NR+5)    :: Flux_g !3/R**0.5*d/dR(Y)
        double precision, dimension(NR+5)    :: RHS !RHS of gas diffusion equation

        !save current surface density

        s_gas_old=s_gas 
        !calculate Y

        do i=is-1,ie+1
           Yg(i)=viscosity(i)*s_gas(i)*Rb(i)**0.5
        end do

        !calculate the flux variable

        do i=is,ie+1
           Flux_g(i)=(3.0/(Ra(i)**0.5))*(Yg(i)-Yg(i-1))/dRb(i)
        end do

        !calculate RHS
        do i=is,ie
           RHS(i)=(g2a(i+1)*Flux_g(i+1)-g2a(i)*Flux_g(i))/DVa(i)+s_gas_source(i)
        end do

        !do update to gas surface density
        !Added 1st Oct also do torque update here
        do i=is,ie
           s_gas(i)=s_gas_old(i)+delta_t*RHS(i)
        end do

        return
      end subroutine advance_gas

      subroutine boundary_gas

        use constants
        use user_mod

        implicit none

        integer      ::   i

        double precision   :: RHS

        !Inner Boundary ccccccccccccccccccccc

        if (in_type_g .eq. 1) then
          !zero shear inner boundary
          s_gas(is-2:is-1)=0.0
        elseif (in_type_g .eq. 9) then

          call user_inner_BC_g

        endif

        !cccccccccccccccccccccccccccccccccccccc

        !Outer Boundary cccccccccccccccccccccc

        
        if (out_type_g .eq. 1) then
        !zero shear
           s_gas(ie+1:ie+2)=0.0
         
        elseif (out_type_g .eq. 2) then
        !constant mdot in
           RHS=-Boundary_out_g*dRb(ie+1)/(6*pi*sqrt(Ra(ie+1))) 
           RHS=RHS+viscosity(ie)*sqrt(Rb(ie))*s_gas(ie)
           s_gas(ie+1)=RHS/(viscosity(ie+1)*sqrt(Rb(ie+1)))

        elseif (out_type_g .eq. 9) then

          call user_outer_BC_g

        end if

        !ccccccccccccccccccccccccccccccccccccc

        !check for suface density below base
        do i=is-2,ie+2
           if (s_gas(i) .lt. s_base) s_gas(i)=s_base
        end do

        return
      end subroutine boundary_gas

      subroutine gas_velocity
        
        use constants

        implicit none

        integer    :: i
        double precision :: diff

        !computes the gas velocity of the a grid
        do i=is-1,ie+2
           diff=(viscosity(i)*s_gas(i)*sqrt(Rb(i))-viscosity(i-1)*s_gas(i-1)*sqrt(Rb(i-1)))/dRb(i)
           u_gas(i)=-6d0/(sqrt(Ra(i))*(s_gas(i)+s_gas(i-1)))*diff
        end do

        return
      end subroutine gas_velocity

    end module gas_mod
        

           
