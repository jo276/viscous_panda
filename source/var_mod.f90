!cccccccccccc PANDA 1D viscous evolution code cccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   VARIABLE MODULE: HOLDS ALL THE VARIABLES FOR EVOULUTION
!c
!c   last modified by JO: 27-07-2014
!c   
!c   v0.1
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

	module var_mod

		implicit none

		!physics logicals
		logical             ::   use_planet   =.false. !include planets
		logical             ::   use_dust     =.false. !include dust
		logical             ::   use_temp     =.false. !evolve temperature equation
		logical             ::   use_birnstiel=.false. ! use birnstiel et al. 2012, simple dust evolution model
		logical             ::   use_birnstiel_with_growth=.false. ! include grwoth from monomer size in birnstiel model

		! controls & switches
		integer             ::   vis_type     =0 !type of viscosity
		integer             ::   T_type       =0 !type of temperature profile to use
		integer             ::   source_type_g=0 !type of source term (gas)
		integer             ::   source_type_d=0 !type of source term (dust)
		integer             ::   adv_method   =1 !advecion method
		integer             ::   out_type_g   =1 !type of outer boundary (gas)
		integer             ::   in_type_g    =1 !type of inner boundary (gas)
		integer             ::   out_type_d   =1 !type of outer boundary (dust)
		integer             ::   in_type_d    =1 !type of inner boundary (dust)
		integer             ::   Ndump        =10 !dump output to file every Ndump steps
	    integer             ::   Nmax         =100 !max number of steps
		integer             ::   Nsizes       =1!number of sizes
        integer             ::   Nspecies     =1!number of dust species

		logical             ::   user_dump    =.false. ! user specified output
		logical             ::   verbose      =.true. ! verbose output

		double precision    ::   Tmax         =1d7 !max integration time


		!simulation parameters (gas)
		double precision    ::   nu_0         =0.0 !reference viscosity
		double precision    ::   alpha        =0.0 !viscous alpha
		double precision    ::   nu_PL        =0.0 !viscosity power-law
		double precision    ::   Temp_PL      =0.0 !temperature power-law
		double precision    ::   T_0          =0.0 !reference temperature
		double precision    ::   HoverR_0     =0.0 !reference HoverR
		double precision    ::   R_0          =0.0 !reference Radii

		!simulation parameters (dust)
		double precision    ::   cons_d_dens  =1.0 !fixed dust density for all species
		double precision    ::   min_dust     =1d-50 !min dust particle size [cm]
		double precision    ::   max_dust     =1d-50 !max dust particle size [cm]
		double precision    ::   dust_PL      =3.5 ! power-law for dust distribution initilisation (n(a)da~a^PL)
		double precision    ::   SC_cons      =1   ! constant schmidt number if wanted
		double precision    ::   amonomer     =1e-5 ! monomer size need for birnstiel dust evolution model
		double precision    ::   ufrag        =1e3 ! fragmentation velocity in birnstiel model (10m/s)
		double precision    ::   tgrow_initial=0. ! intial growth time before sim starts

		!Boundary condition parameters
		double precision    ::   Boundary_out_g =0.0 !Parameter at outer boundary (gas)
		double precision    ::   Boundary_in_g  =0.0 !Parameter at inner boundary (gas)
		double precision    ::   Boundary_out_d =0.0 !Parameter at outer boundary (dust)
		double precision    ::   Boundary_in_d  =0.0 !Parameter at inner boundary (dust)

		! global simulation variables
		double precision    ::   time      =0.0
		double precision    ::   time_dust =0.0 ! time to start evolving dust population
		double precision    ::   delta_t   =0.0
		double precision    ::   CFL       =1.0
		double precision    ::   dt_vis    =1d50
		double precision    ::   dt_torque =1d50
		double precision    ::   dt_leak   =1d50
		double precision    ::   dt_dust   =1d50
		double precision    ::   dt_user   =1d50 

		!user arrays
		double precision, dimension(100) :: x_ctrl=0.0
		integer         , dimension(100) :: x_integer_ctrl=0
		logical         , dimension(100) :: x_logical_ctrl=.false.

		! gas arrays
		double precision, dimension(:), allocatable, save  :: s_gas,s_gas_old !gas surface density
    	double precision, dimension(:), allocatable, save  :: s_gas_source !surface density_source
    	double precision, dimension(:), allocatable, save  :: viscosity !kinematic viscosity
    	double precision, dimension(:), allocatable, save  :: temperature
    	double precision, dimension(:), allocatable, save  :: H_g !gas scale height
    	double precision, dimension(:), allocatable, save  :: u_gas !gas radial velocity defined on a grid 
    	double precision, dimension(:), allocatable, save  :: v_kep_a !Keplerian angular velocity (a grid)
    	double precision, dimension(:), allocatable, save  :: v_kep_b !Keplerian angular velocity (b grid)

  		! dust arrays

  		double precision, dimension(:), allocatable, save     :: size_max !max size in dust distrubtion, used in birnstiel dust evolution
  		double precision, dimension(:), allocatable, save     :: f_m ! max flux parameter for birnstiel model
  		double precision, dimension(:), allocatable, save     :: afrag, adrift, adf, agrow ! max grain size limits in birnstiel model

  		double precision, dimension(:,:), allocatable, save   :: rho_d !dust particle density [cm^-3]
        double precision, dimension(:,:), allocatable, save   :: sizes ! sizes of the particles [cm]
        double precision, dimension(:,:), allocatable, save   :: mass_d ! mass of a dust particle
        double precision, dimension(:,:), allocatable, save   :: weights ! weights associated with PL dust distubtions

        double precision, dimension(:,:,:), allocatable, save :: Schmidt ! ratio of visosicty to dust diffusion
  		double precision, dimension(:,:,:), allocatable, save :: tstop
        double precision, dimension(:,:,:), allocatable, save :: s_dust, s_dust_old !dust surface density
        double precision, dimension(:,:,:), allocatable, save :: s_dust_source !dust source
        double precision, dimension(:,:,:), allocatable, save :: u_dust ! dust velocity defined on a grid
        double precision, dimension(:,:,:), allocatable, save :: Conc !dust concentration 
        double precision, dimension(:,:,:), allocatable, save :: RHS_dust_adv !update term due to advection
        double precision, dimension(:,:,:), allocatable, save :: s_star !surface densities calcualted on a grid






	end module var_mod