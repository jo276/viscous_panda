program panda_driver
	! driver program for code 
	use constants
	use grid_mod
	use gas_mod
	use var_mod
	use io_mod
	use user_mod
#ifdef wdust
	use dust_mod
#endif

	implicit none

	integer      ::   i
	!setup
	call read_inputs
	call make_grid
	call alloc_basic_gas
	call initialise_gas
	call calc_viscosity
	call calc_scale_height
#ifdef wdust
	call alloc_dust
	call initialise_dust
#endif
	call user_setup
	call boundary_gas

	!setup complete
	if (verbose) call splash
	!initial output
	call check_and_do_output(0)
	!integration
	do i=1,Nmax
		call calc_viscosity
		call calculate_timestep
#ifdef wdust
		call gas_velocity
	    call advance_dust
#endif
#ifndef nogasevolve	
		call advance_gas
		call boundary_gas
#endif
		!user extras
		call user_extra_evolve
		!update time
		time=time+delta_t
		!output
		call check_and_do_output(i)
		!check for max time
		if (time .gt. Tmax) exit
	end do
	!final output
	call check_and_do_output(0)
	if (verbose) write (*,*) "Finished Successfully"

end program panda_driver

subroutine calculate_timestep

	use gas_mod
	use user_mod
#ifdef wdust
	use dust_mod
#endif

	implicit none
	!get and combine time-steps
	call gas_timestep	
#ifdef wdust
	call dust_timestep
#endif
	call user_timestep
	!calculate actual time-step
	delta_t=CFL*(1d0/sqrt((1.0/dt_vis)**2.0+(1.0/dt_dust)**2.0 &
		+(1.0/dt_user)**2.0))
	return
end subroutine calculate_timestep