!ccccccccccccccccc 1D viscous evolution code ccccccccccccccccc
!c
!c   written by JO Feb 2013 
!c
!c   DUST MODULE: HOLDS DUST VARIABLES AND DOES DUST EVOLUTION
!c
!c
!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

  module dust_mod

    use grid_mod
    use var_mod
    use constants

    implicit none


    contains

      subroutine alloc_dust
        implicit none
        ! allocate dust arrays
        allocate(rho_d(Nsizes,Nspecies))
        allocate(sizes(Nsizes,Nspecies))
        allocate(mass_d(Nsizes,Nspecies))
        allocate(weights(Nsizes,Nspecies))

        allocate(Schmidt(NR+5,Nsizes,Nspecies))
        allocate(tstop(NR+5,Nsizes,Nspecies))
        allocate(s_dust(NR+5,Nsizes,Nspecies))
        allocate(s_dust_old(NR+5,Nsizes,Nspecies))
        allocate(s_dust_source(NR+5,Nsizes,Nspecies))
        allocate(u_dust(NR+5,Nsizes,Nspecies))
        allocate(Conc(NR+5,Nsizes,Nspecies))
        allocate(RHS_dust_adv(NR+5,Nsizes,Nspecies))
        allocate(s_star(NR+5,Nsizes,Nspecies))
        if (use_birnstiel) then
        	allocate(afrag(NR+5))
        	allocate(adrift(NR+5))
        	allocate(adf(NR+5))
        	allocate(agrow(NR+5))
          	allocate(size_max(NR+5))
          	allocate(f_m(NR+5))
        endif
        return
      end subroutine alloc_dust


      subroutine initialise_dust
        !this subroutine sets up the dust variables and the intial dust surface density
        use gas_mod

        implicit none
        
        integer            :: i,j,k

        double precision   :: DS,store1,store2,store3
        
        double precision   :: binsize(Nsizes)

        do k=1,Nspecies
           do j=1,Nsizes
              rho_d(j,k)=cons_d_dens !g/cm3
              sizes(j,k)= 1e-7 !cm
              do i=is-1,ie+1
                 s_dust(i,j,k)=sd_base
                 Schmidt(i,j,k)=SC_cons
              end do
           end do
        end do

        if (use_birnstiel) then
          size_max(:)= amonomer

          if (Nspecies .gt. 1) then
            print*, "Error, Birnstiel Method requires Nspecies=1"
            stop
          elseif (Nsizes .gt. 1) then
            print*, "Error, Birnstiel Method reuires Nsizes=1"
            stop
          endif

        endif    

        return
      end subroutine initialise_dust


      subroutine dust_source

        !this subroutine calculates the dust source term
        use gas_mod
        use user_mod

        implicit none

        integer     :: i,j,k

        integer     :: i_insert !index where dust is inserted

        double precision, dimension(Nsizes)    :: Int_sizes
        double precision                       :: Int_total

        s_dust_source=0.

        call user_source_term_d

        return
      end subroutine dust_source

      subroutine advance_dust

        implicit none

        !driver subroutine to perform dust update
        if (time >= time_dust) then
          s_dust_old=s_dust
          if (use_birnstiel) call birnstiel_size_update
          call dust_velocity
          call dust_advection_terms
          call dust_source
          call combined_dust_update
          call boundary_dust
        endif

        return
      end subroutine advance_dust

      subroutine dust_velocity

        !this subroutine computes the dust velocity 
        !it requires the gas velcity calculated by gas_velocity in gas_mod

        use gas_mod

        implicit none

        integer  :: i,j,k

        double precision, dimension(NR+5)          :: eta !logarithmic pressure derivative term
        double precision, dimension(NR+5)          :: DlogSDlogR
        double precision, dimension(NR+5)          :: DlogHDlogR
        double precision, dimension(NR+5)          :: tstop0 !stopping time for monomers

        double precision                           :: u0, u1, fm_a

        if (use_birnstiel) then

          do i=is,ie+1
            tstop0(i)=pi*cons_d_dens*amonomer/(s_gas(i)+s_gas(i-1))+1d-20
            tstop(i,1,1)=pi*cons_d_dens*size_max(i)/(s_gas(i)+s_gas(i-1))+1d-20
          end do

          do i=is,ie+1
            DlogSDlogR(i)=(log(s_gas(i)+s_base)-log(s_gas(i-1)+s_base))/(log(Rb(i))-log(Rb(i-1)))
            DlogHDlogR(i)=(log(H_g(i))-log(H_g(i-1)))/(log(Rb(i))-log(Rb(i-1)))
          end do

          !calculate eta following Alexander et al. (2007)

          do i=is,ie+1
            eta(i)=-(((H_g(i)+H_g(i-1))/(2.0*Ra(i)))**2d0)*(DlogSDlogR(i)+(DlogHDlogR(i)-3.0))
          end do

          ! calculate velocity
          do i=is,ie+1
            u0=u_gas(i)/tstop0(i)-eta(i)*v_kep_a(i)
            u0=u0/(1.0/tstop0(i)+tstop0(i))

            u1=u_gas(i)/tstop(i,1,1)-eta(i)*v_kep_a(i)
            u1=u1/(1.0/tstop(i,1,1)+tstop(i,1,1))

            fm_a=(f_m(i)+f_m(i-1))/2.

            u_dust(i,1,1)=(1.-fm_a)*u0+fm_a*u1

          end do

        else        
          !calculate dimensionless stopping time
          do k=1,Nspecies
             do j=1,Nsizes
                do i=is,ie+1
                   tstop(i,j,k)=pi*rho_d(j,k)*sizes(j,k)/(s_gas(i)+s_gas(i-1))+1d-20 ! to prevent fpe
                end do
            end do
          end do
        
          !calculate logarithmic derivative of gas surface_density and scale height
        
          do i=is,ie+1
            DlogSDlogR(i)=(log(s_gas(i)+s_base)-log(s_gas(i-1)+s_base))/(log(Rb(i))-log(Rb(i-1)))
            DlogHDlogR(i)=(log(H_g(i))-log(H_g(i-1)))/(log(Rb(i))-log(Rb(i-1)))
          end do


          !calculate eta following Alexander et al. (2007)

          do i=is,ie+1
            eta(i)=-(((H_g(i)+H_g(i-1))/(2.0*Ra(i)))**2d0)*(DlogSDlogR(i)+(DlogHDlogR(i)-3.0))
          end do


          do k=1,Nspecies
             do j=1,Nsizes
                do i=is,ie+1
                   u_dust(i,j,k)=u_gas(i)/tstop(i,j,k)-eta(i)*v_kep_a(i)
                   u_dust(i,j,k)=u_dust(i,j,k)/(1.0/tstop(i,j,k)+tstop(i,j,k))
                end do
            end do
          end do

        endif
        
        return

      end subroutine dust_velocity

      subroutine dust_timestep
        ! calculates the time-step associted with dust advection and diffusion
        ! advection time-scale is just a courant condition
        use gas_mod

        implicit none

        integer                   ::   i,j,k
        double precision          ::   dt_adv, dt_diff, dt_temp1, dt_temp2


        dt_adv=1d100 !set to very large value
        dt_diff=1d100

        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie+1
                 !advection time-scale
                 dt_temp1=(Rb(i)-Rb(i-1))/abs(u_dust(i,j,k))
                 dt_temp2=dRa(i)**2.0/(6.0*viscosity(i))*Schmidt(i,j,k)
                 if (dt_temp1 .lt. dt_adv)  dt_adv =dt_temp1
                 if ((dt_temp2 .lt. dt_diff) .and. (i .le. ie)) dt_diff=dt_temp2
              end do
           end do
        end do

        !calculate actual time-step
        dt_dust=1.0/(sqrt((1.0/dt_adv**2.0)+(1.0/dt_diff**2.0)))

        return

      end subroutine dust_timestep

      subroutine combined_dust_update
        ! calculates the evolution of dust due to diffuison term
        ! then updates the dust equation
        use gas_mod

        implicit none

        integer             :: i,j,k
        double precision, dimension(NR+5,Nsizes,Nspecies)   :: Div_F, Diff

        !first calculate the diffusion constant
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie+1
                 Diff(i,j,k)=-(viscosity(i)+viscosity(i-1))*(s_gas(i)+s_gas(i-1))/(4.0*Schmidt(i,j,k))
              end do
           end do
        end do
        !next calculate concetraion
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is-1,ie+1
                 Conc(i,j,k)=s_dust(i,j,k)/(s_gas(i)+s_base)
              end do
           end do
        end do
        !now calculate divegence of flux
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie
                 Div_F(i,j,k)=g2a(i+1)**2.0*Diff(i+1,j,k)/(DVa(i))*(Conc(i+1,j,k)-Conc(i,j,k))/DVb(i+1) &
                      -g2a(i)**2.0*Diff(i,j,k)/(DVa(i))*(Conc(i,j,k)-Conc(i-1,j,k))/DVb(i)
              end do
           end do
        end do

        !update dust density !added dust advection to remove operator splitting
        do k=1,Nspecies
           do j=1,Nsizes
               do i=is,ie
                  s_dust(i,j,k)=s_dust(i,j,k)+delta_t*(-Div_F(i,j,k)-RHS_dust_adv(i,j,k)+s_dust_source(i,j,k))
               end do               
           end do
        end do

        return
      end subroutine combined_dust_update
     
      subroutine dust_advection_terms

        !this subroutine calculates the term necessary for dust diffusion

        use gas_mod

        implicit none

        integer            :: i,j,k

        double precision, dimension(NR+5,Nsizes,Nspecies)   ::  flux,dq
        double precision                                    ::  Dq_p,Dq_m !1st order derivatives of S

        if (adv_method .eq. 2) then
           !construct van-leer slopes and limiters following stone et al. (1992)
           do k=1,Nspecies
              do j=1,Nsizes
                 do i=is-1,ie+1
                    Dq_p=(s_dust(i+1,j,k)-s_dust(i,j,k))/dRb(i+1)
                    Dq_m=(s_dust(i,j,k)-s_dust(i-1,j,k))/dRb(i)
                    if ((Dq_p*Dq_m) .gt. 0) then
                       dq(i,j,k)=2.0*(Dq_m*Dq_p)/(Dq_m+Dq_p)
                    else
                       dq(i,j,k)=0.0
                    endif
                 end do
              end do
           end do
        endif

        ! calculate the density on the grid boundaries
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie+1
                 if (u_dust(i,j,k) .gt. 0.0) then
                    if (adv_method .eq. 1) then
                       ! 1st order reconstruction
                       s_star(i,j,k)=s_dust(i-1,j,k)
                    elseif (adv_method .eq. 2) then
                       !2nd order reconstruction
                       s_star(i,j,k)=s_dust(i-1,j,k)+(dRb(i)-u_dust(i,j,k)*delta_t)*dq(i-1,j,k)/2.0
                    endif
                 else
                    if (adv_method .eq. 1) then
                       !1st order reconstruction
                       s_star(i,j,k)=s_dust(i,j,k)
                    elseif (adv_method .eq. 2) then
                       !2nd order reconstruction
                       s_star(i,j,k)=s_dust(i,j,k)-(dRb(i+1)+u_dust(i,j,k)*delta_t)*dq(i,j,k)/2.0
                    endif
                 end if
              end do
           end do
        end do

        !calculate the flux
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie+1
                 flux(i,j,k)=s_star(i,j,k)*u_dust(i,j,k)*g2a(i)
              end do
           end do
        end do

        do k=1,Nspecies
           do j=1,Nsizes
              do i=is,ie
                 RHS_dust_adv(i,j,k)=(flux(i+1,j,k)-flux(i,j,k))/dVa(i)
              end do
           end do
        end do

        return
      end subroutine dust_advection_terms


      subroutine boundary_dust
        ! computes the boundary updates due to the dust

        use gas_mod
        use user_mod

        implicit none

        integer :: i,j,k

 !ccccccccccccccccccc Inner Boundary ccccccccccccccccccccc
        do k=1,Nspecies
          do j=1,Nsizes
            if (in_type_d .eq. 1) then
              !free outflow
              s_dust(is-2:is-1,j,k)=0.0
            elseif (in_type_d .eq. 9) then

              call user_inner_BC_d

            endif
          end do
        end do
 !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

 !ccccccccccccccccccc Outer Boundary cccccccccccccccccccccc

        do k=1,Nspecies
           do j=1,Nsizes
              !outer boundary
              if (out_type_d .eq. 1) then
                 !free outflow
                 s_dust(ie+1,j,k)=0d0
              elseif (out_type_d .eq. 2) then
                 !constant mdot in of dust

              elseif (out_type_d .eq. 3) then 
                 !peg to gas value
                 s_dust(ie+1,j,k)=Boundary_out_d*s_gas(ie+1)
              elseif (out_type_d .eq. 9) then

                 call user_outer_BC_d

              endif
           end do
        end do
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        ! check floor value
        do k=1,Nspecies
           do j=1,Nsizes
              do i=is-2,ie+2
                 if (s_dust(i,j,k).lt.sd_base) s_dust(i,j,k)=sd_base
              end do
           end do
        end do


        return
      end subroutine boundary_dust

      subroutine birnstiel_size_update

        ! updates the particles sizes in the birstiel et al. 2012 growth method

        use gas_mod

        integer            ::    i

        double precision   ::    St_df, tgrow, alpha_local
        double precision   ::    cs, dlogP_dlogR, dlogP_dlogR_m, dlogP_dlogR_p
        double precision   ::    Pm, Pp, P0 
        double precision, parameter :: f_f=0.37
        double precision, parameter :: f_d=0.55
        double precision, parameter :: f_m_f=0.75
        double precision, parameter :: f_m_d=0.97

        do i=is-1,ie+1
          ! fragmentation threshold
          cs=sqrt(kb*temperature(i)/(mmw*mh))
          alpha_local=v_kep_b(i)*viscosity(i)/(Rb(i)*cs**2.)
          afrag(i)=f_f*(2./(3.*pi))*s_gas(i)/(cons_d_dens*alpha_local)*ufrag**2./(cs**2.)
          
          ! drift threshold 
          Pm=s_gas(i-1)*kb*temperature(i-1)/(mmw*mh)/H_g(i-1)
          P0=s_gas(i)*kb*temperature(i)/(mmw*mh)/H_g(i)
          Pp=s_gas(i+1)*kb*temperature(i+1)/(mmw*mh)/H_g(i+1)
          dlogP_dlogR_p=((log(Pp)-log(P0))/(log(Rb(i+1))-log(Rb(i))))
          dlogP_dlogR_m=((log(P0)-log(Pm))/(log(Rb(i))-log(Rb(i-1))))
          dlogP_dlogR = (dlogP_dlogR_p+dlogP_dlogR_m)/2.
          adrift(i)=f_d*2.*S_dust(i,1,1)/(pi*cons_d_dens)*(Rb(i)/H_g(i))**2./abs(dlogP_dlogR)

          ! framgentation drift threshold
          St_df=0.5*(Rb(i)/H_g(i))*(ufrag/cs)/abs(dlogP_dlogR)
          adf(i) = St_df * 2.*s_gas(i)/(pi*cons_d_dens)

          ! correct max size
          size_max(i)=min(afrag(i), adrift(i), adf(i))

          if (size_max(i) .eq. adrift(i)) then
            f_m(i)=f_m_d
          else 
            f_m(i)=f_m_f
          endif

          ! now check for growth
          if (use_birnstiel_with_growth) then
          	tgrow=S_gas(i)/S_dust(i,1,1)*(Rb(i)/v_kep_b(i))
          	if ((time-time_dust+tgrow_initial*sec_to_year)/tgrow < 17) then
				!to prevent overflow in exp
          		agrow(i)=amonomer*exp((time-time_dust+tgrow_initial*sec_to_year)/tgrow)
          	else
          		agrow(i)=amonomer*exp(17.)
          	endif
          	size_max(i)=min(size_max(i),agrow(i))
          endif
          	size_max(i)=max(size_max(i),amonomer)
        end do

        return

      end subroutine birnstiel_size_update


    end module dust_mod
