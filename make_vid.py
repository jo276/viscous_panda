import numpy as np
import panda_tools as pt
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
n=100

os.system("rm -f vid.mp4")

for i in range(0,n+1):
	data=pt.read_gas(i)
	plt.loglog(data.radius/1.5e13,data.surfdens)
	plt.ylim(1e-5,1e5)
	plt.xlim(0.1,1000)
	plt.title("Time = {0:01} Myr".format(data.time/31556926/1e6-0.1))
	plt.xlabel("Radius [AU]")
	plt.ylabel("Surface Density [g/cm2]")
	plt.savefig("fig{0:01}.png".format(i))
	plt.close()

os.system("ffmpeg -i fig%d.png vid.mp4")
os.system("rm -f fig*.png")
